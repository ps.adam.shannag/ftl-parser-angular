import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { SplitterModule } from 'primeng/splitter';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { ButtonGroupComponent } from './components/button-group/button-group.component';
import { UploadSplitterComponent } from './components/upload-splitter/upload-splitter.component';
import { ParseOutputComponent } from './components/parse-output/parse-output.component';
import { TooltipModule } from 'primeng/tooltip';
import { ToastModule } from 'primeng/toast';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ButtonGroupComponent,
    UploadSplitterComponent,
    ParseOutputComponent,
  ],
  imports: [
    BrowserModule,
    SplitterModule,
    FileUploadModule,
    HttpClientModule,
    InputTextareaModule,
    ButtonModule,
    TooltipModule,
    ToastModule,
    BrowserAnimationsModule,
    InputSwitchModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
