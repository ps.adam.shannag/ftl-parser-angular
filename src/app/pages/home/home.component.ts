import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  ftlTemplate: string = '';
  msgTemplate: string = '';
  output: string = '';

  clear() {
    this.ftlTemplate = '';
    this.msgTemplate = '';
  }

  update(event: { data: string; field: number }) {
    if (event.field === 0) this.ftlTemplate = event.data;
    else this.msgTemplate = event.data;
  }

  setOutput(output: string) {
    this.output = output;
  }
}
