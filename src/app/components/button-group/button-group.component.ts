import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FtlService } from '../../services/ftl.service';

@Component({
  selector: 'app-button-group',
  templateUrl: './button-group.component.html',
  styleUrls: ['./button-group.component.scss'],
})
export class ButtonGroupComponent {
  @Input() ftl = '';
  @Input() msg = '';
  @Output() clearFields = new EventEmitter<string>();
  @Output() updateOutput = new EventEmitter<string>();

  constructor(private ftlService: FtlService) {}

  canParse() {
    if (this.ftl === '' || this.msg === '') return true;
    return false;
  }

  canCancel() {
    if (this.ftl !== '' || this.msg !== '') return false;
    return true;
  }
  parse() {
    this.ftlService
      .getParsedFtlData({
        template: this.ftl,
        message: this.msg,
      })
      .subscribe((ftlData) => {
        this.updateOutput.emit(ftlData);
      });
  }

  clear() {
    this.clearFields.emit();
  }
}
