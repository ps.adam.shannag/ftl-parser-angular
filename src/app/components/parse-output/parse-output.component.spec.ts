import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParseOutputComponent } from './parse-output.component';

describe('ParseOutputComponent', () => {
  let component: ParseOutputComponent;
  let fixture: ComponentFixture<ParseOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParseOutputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ParseOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
