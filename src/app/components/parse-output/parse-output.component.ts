import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-parse-output',
  templateUrl: './parse-output.component.html',
  styleUrls: ['./parse-output.component.scss'],
  providers: [MessageService],
})
export class ParseOutputComponent implements OnInit {
  @Input() text = '';
  constructor(private messageService: MessageService) {}

  ngOnInit(): void {}

  copy() {
    navigator.clipboard.writeText(this.text).then(() =>
      this.messageService.add({
        severity: 'info',
        summary: 'Info',
        detail: 'Text copied!',
      })
    );
  }
}
