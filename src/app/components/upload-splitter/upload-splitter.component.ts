import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-upload-splitter',
  templateUrl: './upload-splitter.component.html',
  styleUrls: ['./upload-splitter.component.scss'],
})
export class UploadSplitterComponent {
  checked: boolean = true;
  @Input() ftl = '';
  @Input() msg = '';

  @Output() onFileUploaded = new EventEmitter<{
    data: string;
    field: number;
  }>();
  updateText() {
    this.onFileUploaded.emit({ data: this.ftl, field: 0 });
    this.onFileUploaded.emit({ data: this.msg, field: 1 });
  }
  upload(event: any, field: string) {
    const file: File = event.files[0];
    const fileReader = new FileReader();
    fileReader.readAsText(file, 'UTF-8');
    fileReader.onload = (evt) => {
      if (field === 'FTL') {
        this.ftl = evt.target?.result as string;
        this.onFileUploaded.emit({ data: this.ftl, field: 0 });
      }
      if (field === 'MSG') {
        this.msg = evt.target?.result as string;
        this.onFileUploaded.emit({ data: this.msg, field: 1 });
      }
    };
  }
}
