import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadSplitterComponent } from './upload-splitter.component';

describe('UploadSplitterComponent', () => {
  let component: UploadSplitterComponent;
  let fixture: ComponentFixture<UploadSplitterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadSplitterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadSplitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
