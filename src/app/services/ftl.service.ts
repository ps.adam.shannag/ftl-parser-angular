import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { catchError, switchMap } from 'rxjs/operators';
import config from '../_config_files/baseUrl.json';

@Injectable({
  providedIn: 'root',
})
export class FtlService {
  baseUrl: string = config.baseUrl;
  constructor(private http: HttpClient) {}

  getParsedFtlData(data: { template: string; message: string }) {
    return this.http.post<{ output: string }>(this.baseUrl, data).pipe(
      catchError((error) => {
        return of(error.error);
      }),
      switchMap((response) => {
        return of(response.output);
      })
    );
  }
}
