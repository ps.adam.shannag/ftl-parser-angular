import { TestBed } from '@angular/core/testing';

import { FtlService } from './ftl.service';

describe('FtlService', () => {
  let service: FtlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FtlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
