# Ftl Parser App with Angular

Front-end application made with Angular using [spring boot ftl parser api](https://gitlab.com/ps.adam.shannag/ftl-parser).

## Smaples:
![1.PNG](./samples/1.PNG)
![2.PNG](./samples/2.PNG)
![3.PNG](./samples/3.PNG)
### Error sample
![4.PNG](./samples/4.PNG)
